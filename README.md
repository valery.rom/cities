# Cities

Тестовое задание на позицию бэкенд (ориентировочно 4 часа):

1. Написать скрипт на python3, который получает данные по API для страны Россия https://dev.vk.com/ru/method/database.getCities

2. Скрипт должен записывать города в mariadb базу данных в таблицу городов. Переписывать все поля не нужно, только id города, название города и id региона

3. Окончание работы либо после обработки всех данных, либо после нажатия ctrl+с

4. Оцениваются - скорость работы скрипта, понятность stdout, отсутствие ошибок и дубликатов в записанных данных, отсутствие ошибок во время работы

5. Для проверки нужно передать репозиторий с кодом на github, где есть main.py, requirements.txt, init.sql


## Системные требования

- git
- Docker


## Локальная разработка

1. Добавить переменные окружения в файл ./config/.env по примеру из ./config/example.env


2. Команды для разработки в docker compose

```shell
# Создать и запустить. Также для пересоздания образа при появлении изменений в Dockerfile, start, Pipfile
docker compose up --build

# Остановить
ctrl + C

# Удалить контейнеры
docker compose down

# Запустить
docker compose up

# Отобразить работающие контейнеры
docker ps

# Запуск в контейнере bash shell
docker exec -it <container name> bash

# delete the object created by docker-compose when you want to clean the development environment and recreate it from scratch.
docker-compose down --rmi all -v --remove-orphans


```

3. Настроить интерпретатор питона из докера в IDE

Докер образ: **se-query-server:latest**

PyCharm: https://www.jetbrains.com/help/pycharm/using-docker-compose-as-a-remote-interpreter.html#example

4. Команды разработчика

```shell

# Форматеры и линтеры
black ./app

# Alembic

alembic revision -m "Add search content type" --autogenerate
alembic upgrade head
alembic downgrade -1

```

5. Добавление новых пакетов

Добавлять новый пакет в Pipfile с версией, например httpx = "==0.23.0".
Для пересоздания образа и Pipfile.lock использовать `docker compose up --build` или `pipenv lock`


6. Команды для разработки в virtualenv



```shell


pip install pipenv

# Активировать виртуальное окружение
pipenv shell

# Переменные окружения (игнорирование строк начинающихся с '#')
export $(grep -v '^#' config/.env | xargs)

# Установка пакетов
pipenv install --dev

# Создание файла  Pipfile.lock
pipenv lock --dev

# Запуск
python ./app/main.py

```
