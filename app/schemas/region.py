from __future__ import annotations

from pydantic import BaseModel


class RegionBase(BaseModel):
    title: str
    vk_id: int
    country_id: int


class RegionCreate(RegionBase):
    pass


class RegionUpdate(RegionBase):
    pass


class RegionDetail(RegionBase):
    id: int

    class Config:
        from_attributes = True
