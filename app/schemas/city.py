from __future__ import annotations

from pydantic import BaseModel


class CityBase(BaseModel):
    title: str
    vk_id: int
    region_id: int


class CityCreate(CityBase):
    pass


class CityUpdate(CityBase):
    pass


class CityDetail(CityBase):
    id: int

    class Config:
        from_attributes = True
