from __future__ import annotations

from pydantic import BaseModel


class CountryBase(BaseModel):
    title: str
    vk_id: int


class CountryCreate(CountryBase):
    pass


class CountryUpdate(CountryBase):
    pass


class CountryDetail(CountryBase):
    id: int

    class Config:
        from_attributes = True
