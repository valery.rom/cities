import asyncio
import logging

from .clients import VKApiClient
from . import crud, schemas
from .db.models import Country, Region
from .db import db_session

logger = logging.getLogger("app")


class CitiesUpdater:
    def __init__(self):
        self.vk_api_client = VKApiClient()

    async def execute(self):
        logger.info("Starting update \n")

        countries = await self.update_countries()

        for country in countries:
            regions = await self.update_regions(country)

            # regions_step определяет количество асинхронных задач запускаемых одновременно
            # максимальное число определено опытным путем
            # при ошибке из-за превышения количества запросов в секунду задача повторяется через промежуток времени
            regions_step = 4
            for start in range(0, len(regions), regions_step):
                await asyncio.gather(
                    *[self.update_cities(country, region) for region in regions[start : start + regions_step]]
                )

        async with db_session() as db:
            cities_count = await crud.city.count(db)

        logger.info(f"Update finished. Cities in db: {cities_count}")

    async def update_countries(self) -> list[Country]:
        countries_data = await self.vk_api_client.get_countries()
        logger.info(f"Countries fetched: {len(countries_data)}")

        async with db_session() as db:
            await crud.country.bulk_create_having_new_vk_id(
                db, [schemas.CountryCreate(**self.prepare(data)) for data in countries_data]
            )
            countries = await crud.country.get_multi(db)
        return countries

    async def update_regions(self, country: Country) -> list[Region]:
        regions_data = await self.vk_api_client.get_regions(country.vk_id)
        logger.info(f"Country: {country.title} Regions fetched: {len(regions_data)}")

        async with db_session() as db:
            await crud.region.bulk_create_having_new_vk_id(
                db, [schemas.RegionCreate(country_id=country.id, **self.prepare(data)) for data in regions_data]
            )
            regions = await crud.region.get_multi(db)
        return regions

    async def update_cities(self, country: Country, region: Region) -> None:
        step = 100
        offset = 0
        count = step

        while count >= offset:
            cities_data, count = await self.vk_api_client.get_cities(country.vk_id, region.vk_id, offset, step)
            offset += step

            logger.info(f"Region: {region.title} Cities fetched: {len(cities_data)}")

            async with db_session() as db:
                await crud.city.bulk_create_having_new_vk_id(
                    db, [schemas.CityCreate(region_id=region.id, **self.prepare(data)) for data in cities_data]
                )

    def prepare(self, data: dict) -> dict:
        data["vk_id"] = data.pop("id")
        return data
