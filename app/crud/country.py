from app.crud.base import CRUDBase
from app.db.models import Country
from app.schemas import (
    CountryCreate,
    CountryUpdate,
)


class CRUDCountry(CRUDBase[Country, CountryCreate, CountryUpdate]):
    pass


country = CRUDCountry(Country)
