from app.crud.base import CRUDBase
from app.db.models import Region
from app.schemas import RegionCreate, RegionUpdate, RegionDetail


class CRUDRegion(CRUDBase[Region, RegionCreate, RegionUpdate]):
    pass


region = CRUDRegion(Region)
