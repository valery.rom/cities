from app.crud.base import CRUDBase
from app.db.models import City
from app.schemas import (
    CityCreate,
    CityUpdate,
)


class CRUDCity(CRUDBase[City, CityCreate, CityUpdate]):
    pass


city = CRUDCity(City)
