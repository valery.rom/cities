from pathlib import Path
from typing import Optional, Any

from pydantic import Field, MySQLDsn
from pydantic.v1 import BaseSettings, validator, PostgresDsn

APP_DIR = Path(__file__).resolve(strict=True).parent.parent

class CommonSettings(BaseSettings):
    env_state: Optional[str] = Field(None, env="ENV_STATE")

    debug: bool = False

    logging: dict = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "console": {
                "format": (
                    "%(name)-12s %(asctime)s %(levelname)-8s "
                    "%(filename)s:%(funcName)s %(message)s"
                ),
                "datefmt": "%d.%m.%y %H:%M:%S",
            }
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "level": "DEBUG",
                "formatter": "console",
                "stream": "ext://sys.stdout",
            }
        },
        "loggers": {
            "app": {"level": "DEBUG", "handlers": ["console"]},
        },
    }
    
    MYSQL_HOST: str
    MYSQL_PORT: str
    MYSQL_DATABASE: str
    MYSQL_USER: str
    MYSQL_PASSWORD: str
    SQLALCHEMY_DATABASE_URI: str = None
    
    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: str | None, values: dict[str, Any]) -> Any:
        return "mysql+aiomysql://{MYSQL_USER}:{MYSQL_PASSWORD}@{MYSQL_HOST}:{MYSQL_PORT}/{MYSQL_DATABASE}".format(
            MYSQL_USER=values.get("MYSQL_USER"),
            MYSQL_PASSWORD=values.get("MYSQL_PASSWORD"),
            MYSQL_HOST=values.get("MYSQL_HOST"),
            MYSQL_PORT=values.get("MYSQL_PORT"),
            MYSQL_DATABASE=values.get('MYSQL_DATABASE'),
        )
    
    VK_SERVICE_ACCESS_KEY: str

    class Config:
        env_file = str(APP_DIR / "config" / ".env")


class LocalSettings(CommonSettings):
    debug: bool = True

class StageSettings(CommonSettings):
    pass

class ProdSettings(CommonSettings):
    debug: bool = False

settings_index = dict(
    local=LocalSettings,
    stage=StageSettings,
    prod=ProdSettings
)

settings = settings_index.get(CommonSettings().env_state, StageSettings)()
