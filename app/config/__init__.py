"""Модуль для работы с настройками приложения"""

from .config import APP_DIR, settings

__all__ = ["settings", "APP_DIR"]
