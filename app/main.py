import asyncio
import logging.config
import os
import sys
from pathlib import Path

ROOT_DIR = Path(__file__).resolve(strict=True).parent
sys.path.append(os.path.dirname(ROOT_DIR))

from app.config import settings
from app.db.session import db_interface
from app.services import CitiesUpdater


logging.config.dictConfig(settings.logging)
logger = logging.getLogger("app")


async def main():
    cities_updater = CitiesUpdater()
    try:
        await cities_updater.execute()
    except KeyboardInterrupt as err:
        logger.debug("Interrupted")
        sys.exit(os.EX_OK)
    finally:
        logger.debug("Cleaning ...")
        await db_interface.close()


if __name__ == "__main__":
    asyncio.run(main())
