from sqlalchemy import Integer, Column, String, BigInteger, ForeignKey

from .base_class import Base


class Country(Base):
    __tablename__ = "country"

    id = Column(Integer, primary_key=True, index=True)
    vk_id = Column(Integer, index=True, unique=True, nullable=False)
    title = Column(String(length=256), index=True, unique=True, nullable=False)


class Region(Base):
    __tablename__ = "region"

    id = Column(Integer, primary_key=True, index=True)
    vk_id = Column(Integer, index=True, unique=True, nullable=False)
    title = Column(String(length=256), index=True, unique=True, nullable=False)
    country_id = Column(
        Integer,
        ForeignKey("country.id", ondelete="CASCADE"),
        nullable=False,
        index=True,
    )


class City(Base):
    __tablename__ = "city"

    id = Column(BigInteger, primary_key=True, index=True)
    vk_id = Column(Integer, index=True, unique=True, nullable=False)
    title = Column(String(length=256), index=True, nullable=False)
    region_id = Column(
        Integer,
        ForeignKey("region.id", ondelete="CASCADE"),
        nullable=False,
        index=True,
    )
