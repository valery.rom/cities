from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from app.config import settings


class AsyncDb:
    def __init__(self, db_uri=settings.SQLALCHEMY_DATABASE_URI):
        self.engine = None
        self.db_uri = db_uri

    def create_session_maker(self, **kwargs):
        engine_kwargs = {
            "url": self.db_uri,
            "future": True,
            "pool_recycle": 30 * 60,
            "isolation_level": "REPEATABLE READ",
        }
        engine_kwargs.update(**kwargs)

        self.engine = create_async_engine(**engine_kwargs)
        return sessionmaker(
            self.engine,
            future=True,
            expire_on_commit=False,
            class_=AsyncSession,
        )

    async def close(self):
        await self.engine.dispose()


db_interface = AsyncDb()

db_session = db_interface.create_session_maker()
