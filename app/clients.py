import asyncio
import logging

import httpx

from app.config import settings

logger = logging.getLogger("app")


class VKApiClient:
    headers = {
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate",
        "Accept-Language": "en-US,en;q=0.9,ru-RU;q=0.8,ru;q=0.7",
        "Connection": "keep-alive",
    }
    base_url = "https://api.vk.com/method/database."
    countries_url = f"{base_url}getCountries"
    regions_url = f"{base_url}getRegions"
    cities_url = f"{base_url}getCities"

    base_form = dict(
        access_token=settings.VK_SERVICE_ACCESS_KEY,
        need_all=1,
        v="5.131",
    )
    countries_codes = ["RU"]

    def __init__(self):
        self.timeout = httpx.Timeout(10.0, connect=5.0)
        self.transport = httpx.AsyncHTTPTransport(retries=2)
        self.client = httpx.AsyncClient(
            headers=self.headers.copy(),
            transport=self.transport,
            timeout=self.timeout,
        )

    def get_countries_codes_form_data(self):
        return ",".join(self.countries_codes)

    async def get_countries(self) -> list[dict]:
        form_data = dict(code=self.get_countries_codes_form_data(), **self.base_form)
        resp = await self.client.post(self.countries_url, data=form_data)
        resp.raise_for_status()
        data = resp.json()
        return data["response"]["items"]

    async def get_regions(self, vk_country_id: int) -> list[dict]:
        form_data = dict(country_id=vk_country_id, **self.base_form)
        resp = await self.client.post(self.regions_url, data=form_data)
        resp.raise_for_status()
        data = resp.json()
        return data["response"]["items"]

    async def get_cities(
        self,
        vk_country_id: int,
        vk_region_id: int,
        offset: int,
        count: int,
    ) -> tuple[list[dict], int]:
        form_data = dict(country_id=vk_country_id, region_id=vk_region_id, offset=offset, count=count, **self.base_form)
        data = await self.fetch_with_retry(self.cities_url, form_data)
        return data["response"]["items"], data["response"]["count"]

    async def fetch_with_retry(self, url, form_data, sleep_sec=0.5):
        while True:
            resp = await self.client.post(url, data=form_data)
            resp.raise_for_status()
            data = resp.json()
            if not "error" in data and not "error_code" in data:
                break
            else:
                logger.error(f"Vk api error: {data}. Waiting {sleep_sec} sec")
                await asyncio.sleep(sleep_sec)
                sleep_sec += sleep_sec
        return data
